package it.com.atlassian.support.jira.regex.plugin.components;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @since 3.5
 */
public class MigrateDataFuncTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        throw new Exception("MigrateData has no tests!");

    }

}
