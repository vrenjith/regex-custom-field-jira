package ut.com.atlassian.support.jira.regex.plugin.jira.customfields;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.atlassian.support.jira.regex.plugin.jira.customfields.RegexCustomField;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class RegexCustomFieldTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //RegexCustomField testClass = new RegexCustomField();

        throw new Exception("RegexCustomField has no tests!");

    }

}
