package com.atlassian.support.jira.regex.plugin.jira.customfields;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.sisyphus.SisyphusPattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class RegexCustomField extends GenericTextCFType {
    private static final Logger log = LoggerFactory.getLogger(RegexCustomField.class);
    private Hashtable invalidPatterns = new Hashtable();
    private ArrayList<String> replacePatterns = new ArrayList<String>();
    int REGEX_SPEED_MAX = 500;

    public RegexCustomField(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
        super(customFieldValuePersister, genericConfigManager);

        invalidPatterns.put(".*","use non greedy match .*?");
        invalidPatterns.put(".+","use non greedy match .+?");

        //These patterns are replaced in the string before validation so enable the above
        //invalid pattern validation
        replacePatterns.add(".*?");
        replacePatterns.add(".+?");
    }


    @Override
    public void validateFromParams(CustomFieldParams relevantParams, ErrorCollection errorCollectionToAddTo, FieldConfig config) {

        String cfVal;
        try
        {
            cfVal = (String) getValueFromCustomFieldParams(relevantParams);
        }
        catch (FieldValidationException e)
        {
            errorCollectionToAddTo.addError(config.getCustomField().getId(), e.getMessage());
            return;
        }

        if (cfVal != null && cfVal.length() > 0)
        {
            try {
                Pattern p = Pattern.compile(cfVal);
            } catch (PatternSyntaxException e) {
                errorCollectionToAddTo.addError(
                        config.getCustomField().getId(),
                        getI18nBean().getText("regex-custom-field.invalidexpression", cfVal));
                return;
            }

            SisyphusPattern sPattern = new SisyphusPattern();
            sPattern.setRegex(cfVal);

            if(sPattern.testPattern() >= REGEX_SPEED_MAX)
            {
                errorCollectionToAddTo.addError(
                        config.getCustomField().getId(),
                        getI18nBean().getText("regex-custom-field.performance", cfVal, String.valueOf(REGEX_SPEED_MAX)));
            }
            for(String rpl : replacePatterns) {
                cfVal = cfVal.replace(rpl, "x");
            }

            for(Object pattern : invalidPatterns.keySet() ) {
               if(cfVal.contains((String)pattern)){
                   String message = (String) invalidPatterns.get(pattern);
                   errorCollectionToAddTo.addError(
                           config.getCustomField().getId(),
                           getI18nBean().getText("regex-custom-field.errormessage", (String) pattern, message));
               }
            }
        }
    }

    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue,
                                                     final CustomField field,
                                                     final FieldLayoutItem fieldLayoutItem) {
        final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

        // This method is also called to get the default value, in
        // which case issue is null so we can't use it to add currencyLocale
        if (issue == null) {
            return map;
        }

        FieldConfig fieldConfig = field.getRelevantConfig(issue);
        //add what you need to the map here

        return map;
    }
}