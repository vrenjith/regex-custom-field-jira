package com.atlassian.support.jira.regex.plugin.components;

import com.atlassian.core.util.collection.EasyList;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.*;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenTab;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.ofbiz.core.entity.EntityCondition;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class MigrateData implements PluginUpgradeTask {
    private static final Logger log = LoggerFactory.getLogger(MigrateData.class);
    private final CustomFieldManager customFieldManager;
    private final OfBizDelegator delegator;
    private final FieldScreenManager fieldScreenManager;
    private final IssueManager issueManager;
    private final UserManager userManager;
    private final IssueFactory issueFactory;
    private final ProjectManager projectManager;
    private final ConstantsManager constantsManager;
    private final IssueIndexManager issueIndexManager;

    private final String REGEX_FIELD_NAME = "Regular Expression";
    private final String FIELD_DESCRIPTION = "Regular Expression for Atlassian Support issue diagnosis";
    private final String REPORTER_USER_NAME = "rpillai";
    private final String PROJECT_KEY = "JRA";
    private final String ISSUE_TYPE_KEY = "Task";
    private final String ISSUE_SUMMARY = "Update invalid regex";

    public MigrateData(CustomFieldManager customFieldManager, SearchProvider searchProvider, OfBizDelegator delegator,
                       FieldManager fieldManager, FieldScreenManager fieldScreenManager, CommentManager commentManager, IssueManager issueManager, UserManager userManager, IssueFactory issueFactory, ProjectManager projectManager, ConstantsManager constantsManager, IssueIndexManager issueIndexManager) {
        this.customFieldManager = customFieldManager;
        this.delegator = delegator;
        this.fieldScreenManager = fieldScreenManager;
        this.issueManager = issueManager;
        this.userManager = userManager;
        this.issueFactory = issueFactory;
        this.projectManager = projectManager;
        this.constantsManager = constantsManager;
        this.issueIndexManager = issueIndexManager;
    }

    @Override
    public int getBuildNumber() {
        return 1;
    }

    @Override
    public String getShortDescription() {
        return "Migrates the data from the existing text custom field containing regex to this new field";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception {
        CustomFieldType customFieldType = customFieldManager.getCustomFieldType(
                "com.atlassian.support.jira.regex.plugin.regex-custom-field-support:regex-custom-field");
        CustomFieldSearcher customFieldSearcher = customFieldManager.getCustomFieldSearcher(
                "com.atlassian.support.jira.regex.plugin.regex-custom-field-support:text-searcher");

        log.info("Upgrade task starting for migration regex data");
        final Collection<CustomField> customFieldObjects = customFieldManager.getCustomFieldObjectsByName(
                REGEX_FIELD_NAME);
        HashMap<Long,String> invalidRegex = new HashMap<Long, String>();

        CustomField newField = null;
        for(CustomField customField : customFieldObjects){
            if(customField.getCustomFieldType().getKey().equals("com.atlassian.support.jira.regex.plugin.regex-custom-field-support:regex-custom-field")){
                newField = customField;
            }
        }
        //Create a new one only if the field is not existing already in the instance
        if(null == newField) {
            newField = customFieldManager.createCustomField(REGEX_FIELD_NAME,
                    FIELD_DESCRIPTION, customFieldType,
                    customFieldSearcher, EasyList.build(GlobalIssueContext.getInstance()),
                    EasyList.buildNull());
        }
        for(CustomField customField : customFieldObjects){
            if(!customField.getCustomFieldType().getKey().equals("com.atlassian.jira.plugin.system.customfieldtypes:textfield")){
                log.info("Ignoring the custom field of type " + customField.getCustomFieldType().getKey());
                continue;
            }

            //Migrate the database entries
            EntityCondition condition = new EntityExpr("customfield", EntityOperator.EQUALS,customField.getIdAsLong());
            for (GenericValue customFieldValue : delegator.findByCondition("CustomFieldValue", condition, null)) {
                try {
                    Pattern p = Pattern.compile((String) customFieldValue.get("stringvalue"));
                    //Store only if it is a valid expression
                    customFieldValue.put("customfield", newField.getIdAsLong());
                    delegator.store(customFieldValue);
                    log.debug("Migrating value " + (String) customFieldValue.get("stringvalue"));
                } catch (PatternSyntaxException e) {
                    invalidRegex.put((Long) customFieldValue.get("issue"), (String) customFieldValue.get("stringvalue"));
                    log.warn("Invalid regular expression, moving to issue " + (String) customFieldValue.get("stringvalue"));
                    //Remove it if it is not a valid expression
                    delegator.removeValue(customFieldValue);
                }
            }

            //Add the new field to all the screens which the old field was associated to
            List selectedTabs = new ArrayList(fieldScreenManager.getFieldScreenTabs(customField.getId()));
            for(Object o : selectedTabs) {
                FieldScreenTab fieldScreenTab = (FieldScreenTab)o;
                fieldScreenTab.addFieldScreenLayoutItem(newField.getId());
                fieldScreenTab.removeFieldScreenLayoutItem(fieldScreenTab.getFieldScreenLayoutItem(customField.getId()).getPosition());
                log.debug("Migrating field screen " + fieldScreenTab.toString());
            }
            //Store those invalid regex for further manual processing.
            if(invalidRegex.keySet().size() > 0) {
                String content = "||JIRA Issue||Old invalid regex||\n";
                for (Long issue : invalidRegex.keySet()) {
                    content += String.format("|%s|%s|\n",issueManager.getIssueObject(issue).getKey(),invalidRegex.get(issue));
                }
                log.info("Creating new issue for tracking invalid regex");
                MutableIssue newIssue = issueFactory.getIssue();
                newIssue.setSummary(ISSUE_SUMMARY);
                newIssue.setDescription(content);
                newIssue.setReporterId(REPORTER_USER_NAME);
                newIssue.setIssueTypeId(constantsManager.getIssueConstantByName(ConstantsManager.ISSUE_TYPE_CONSTANT_TYPE, ISSUE_TYPE_KEY).getId());
                newIssue.setProjectId(projectManager.getProjectObjByKey(PROJECT_KEY).getId());
                Issue createdIssue = issueManager.createIssueObject(userManager.getUser(REPORTER_USER_NAME), newIssue);
                issueIndexManager.reIndex(createdIssue);
            }
        }
        return Collections.emptyList();
    }

    @Override
    public String getPluginKey() {
        return "com.atlassian.support.jira.regex.plugin.regex-custom-field-support";
    }
}