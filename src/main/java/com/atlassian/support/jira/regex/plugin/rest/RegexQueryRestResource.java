package com.atlassian.support.jira.regex.plugin.rest;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.websudo.WebSudoRequired;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Hashtable;

/**
 * A resource of message.
 */
@Path("/results")
public class RegexQueryRestResource {

    @GET
    @AnonymousAllowed
    @RequiresXsrfCheck
    @WebSudoRequired
    @Produces({MediaType.APPLICATION_JSON})
    @Path ("{projectKey}/{customfieldId}")
    public Response getMessage(@PathParam ("projectKey") String projectKey, @PathParam ("customfieldId") Long customfieldId)
    {
        final Hashtable<Long, RegexQueryRestResourceModel> searchData = new Hashtable<Long, RegexQueryRestResourceModel>();
        try {
            SearchProvider searchProvider = ComponentAccessor.getComponent(SearchProvider.class);
            CustomField customField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject(customfieldId);

            JqlQueryBuilder queryBuilder = JqlQueryBuilder.newBuilder();

            final SearchResults searchResults = searchProvider.search(
                    queryBuilder.where().project(projectKey).and().customField(customfieldId).isNotEmpty().buildQuery()
                    , ComponentAccessor.getJiraAuthenticationContext().getUser(),
                    PagerFilter.getUnlimitedFilter());
            for(Issue issue : searchResults.getIssues())
            {
                searchData.put(issue.getId(), new RegexQueryRestResourceModel(issue.getKey(),issue.getSummary(),
                        (String) issue.getCustomFieldValue(customField)));
            }
            return Response.ok(searchData.values()).build();
        } catch (Exception e) {
            return Response.ok("Internal error").build();
        }
    }
}