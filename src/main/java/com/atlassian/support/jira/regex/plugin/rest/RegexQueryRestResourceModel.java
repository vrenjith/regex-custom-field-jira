package com.atlassian.support.jira.regex.plugin.rest;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "issue")
@XmlAccessorType(XmlAccessType.FIELD)
public class RegexQueryRestResourceModel {
    public RegexQueryRestResourceModel(String key, String summary, String regex) {
        this.key = key;
        this.summary = summary;
        this.regex = regex;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    @XmlElement(name = "key")
    private String key;

    @XmlElement(name = "summary")
    private String summary;

    @XmlElement(name = "regex")
    private String regex;

    public RegexQueryRestResourceModel() {
    }
}